<?php

class Connection
{
	private $dsn = "mysql:host=localhost;dbname=db_ecommerce";

	private $username = "root";

	private $password = "root";

	private $options = [
		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
		PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
	];

	protected $conn;

	public function openConnection()
	{
		try {
			$this->conn = new PDO($this->dsn, $this->username, $this->password, $this->options);
			return $this->conn;
		} catch (PDOException $e) {
			echo "There is some problem in connection: " . $e->getMessage();
		}
	}

	public function closeConnection()
	{
		$this->conn = null;
	}
}