function validate(){
	if(!window.document.getElementById('category-name').value){
		alert("o nome da categoria é obrigatório");
		window.document.getElementById('category-name').focus();
		return false;
	}

	if(!window.document.getElementById('category-url').value){
		alert("a categoria url é obrigatória");
		window.document.getElementById('category-url').focus();
		return false;
	}

	window.document.getElementById('store_category').submit();
}