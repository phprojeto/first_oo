<?php

require_once '../database/Connection.php';

try {
	$database = new Connection();
	$conn = $database->openConnection();
	$sql = "INSERT INTO categories (name, url) VALUES (:name, :url)";

	$stmt = $conn->prepare($sql);
	$result = $stmt->execute([
		'name' => $_POST['name'],
		'url' => $_POST['url']
	]);

	if($result){
		header("location: ../categories.php");
		return;
	}

	header("location: ../addCategory.html");

} catch(PDOException $e) {
	echo $e->getMessage();
}