<?php

require_once '../database/Connection.php';

try {
	$database = new Connection();
	$conn = $database->openConnection();
	$sql = "UPDATE products SET sku = :sku, name = :name, url = :url, price = :price, qty = :qty, description = :description WHERE id = :id";
	$stmt = $conn->prepare($sql);
	$result = $stmt->execute([
		'sku' => $_POST['sku'],
		'name' => $_POST['name'],
		'url' => $_POST['name'],
		'price' => $_POST['price'],
		'qty' => $_POST['quantity'],
		'description' => $_POST['description'],
		'id' => $_GET['id']
	]);
	
	if($result){
		$sqlDel = "DELETE FROM product_categories WHERE product_id = :product_id";
		$stmtDel = $conn->prepare($sqlDel);
		$stmtDel->execute([
			'product_id' => $_GET['id']
		]);

		if(!empty($_POST['category']) && is_array($_POST['category'])){
		foreach ($_POST['category'] as $category) {
			$sql = "INSERT INTO product_categories (product_id, category_id) VALUES (:product_id, :category_id)";
			$stmt = $conn->prepare($sql);
			
			$result = $stmt->execute([
				'product_id' => $_GET['id'],
				'category_id' => $category,
			]);
		}
	}

		header('location: ../products.php');
		return;
	}

	throw new Exception("Error Processing Request", 1);
	
	//$stmt->execute($_POST);
} catch(Exception $e) {
	header('location: ../edit_product.php?id=' . $_GET['id']);
}
