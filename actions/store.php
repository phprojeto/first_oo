<?php
require_once '../database/Connection.php';

try {
	$database = new Connection();
	$conn = $database->openConnection();
	$sql = "INSERT INTO products (sku, name, url, price, description, qty) VALUES (:sku, :name, :url, :price, :description, :qty)";
	$stmt = $conn->prepare($sql);
	
	$result = $stmt->execute([
		'sku' => $_POST['sku'],
		'name' => $_POST['name'],
		'url' => preg_replace('/-{2,}/', '-', $_POST['name']),
		'price' => $_POST['price'],
		'description' => $_POST['description'],
		'qty' => $_POST['quantity']
	]);
	
	$productId = $conn->lastInsertId();

	if(!empty($_POST['category']) && is_array($_POST['category'])){
		foreach ($_POST['category'] as $category) {
			$sql = "INSERT INTO product_categories (product_id, category_id) VALUES (:product_id, :category_id)";
			$stmt = $conn->prepare($sql);
			
			$result = $stmt->execute([
				'product_id' => $productId,
				'category_id' => $category,
			]);
		}
	}
	
	if($result){
		header('location: ../products.php');
		return;
	}

	header('location: ../addProduct.html');
	//$stmt->execute($_POST);
} catch(Exception $e) {
	echo "There is some problem in connection: " . $e->getMessage();
}