<?php

require_once '../database/Connection.php';

try {
	$database = new Connection();
	$conn = $database->openConnection();
	$sql = "UPDATE categories SET name = :name, url = :url WHERE id = :id";
	$stmt = $conn->prepare($sql);
	$result = $stmt->execute([
		'name' => $_POST['name'],
		'url' => $_POST['url'],
		'id' => $_GET['id']	
	]);
	
	if($result){
		header('location: ../categories.php');
		return;
	}

	throw new Exception("Error Processing Request", 1);
	
	//$stmt->execute($_POST);
} catch(Exception $e) {
	header('location: ../edit_category.php?id=' . $_GET['id']);
}
