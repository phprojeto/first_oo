function validate()
{
	if(!window.document.getElementById('sku').value){
		alert('sku é obrigatório');
		window.document.getElementById('sku').focus();
		return false;
	}
	
	if(!window.document.getElementById('name').value){
		alert('name é obrigatório');
		return false;
	}

	if(!window.document.getElementById('price').value){
		alert('price é obrigatório');
		return false;
	}


	window.document.getElementById('store_product').submit();
}

function onlyNumbers(element)
{
	element.value = element.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
}